package entities

type TileNode struct {
	X          uint
	Y          uint
	ContinentX uint
	ContinentY uint
	Type       string
}
