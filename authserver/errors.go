package authserver

const (
	emptyFields      = "empty fields"
	shortCredentials = "credentials are too short"
	wrongAccountInfo = "incorrect login and/or password"
	InternalError    = "internal Error"
	accountExists    = "an account already exists with that information"
	failedSession    = "failed to create session"
	success          = "success"
	notPost          = "only POST allowed!"
	sessionNotFound  = "session not found!"
)
